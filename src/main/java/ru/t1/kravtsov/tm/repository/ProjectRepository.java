package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    public ProjectRepository() {
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void deleteAll() {
        projects.clear();
    }

}
