package ru.t1.kravtsov.tm.api.controller;

public interface ITaskController {

    void displayTasks();

    void createTask();

    void clearTasks();

}
