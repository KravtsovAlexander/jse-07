package ru.t1.kravtsov.tm.model;

public final class Command {

    private String name = "";

    private String argument;

    private String description = "";

    public Command() {
    }

    public Command(final String name, final String argument, final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        if (name != null && !name.isEmpty()) sb.append(name);
        if (argument != null && !argument.isEmpty()) sb.append(", ").append(argument);
        if (description != null && !description.isEmpty()) sb.append(" - ").append(description);

        return sb.toString();
    }

}
