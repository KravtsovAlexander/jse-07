package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.ICommandRepository;
import ru.t1.kravtsov.tm.constant.ArgumentConst;
import ru.t1.kravtsov.tm.constant.TerminalConst;
import ru.t1.kravtsov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Display application commands."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display application arguments."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Show project list."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Remove all projects."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Show task list."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Remove all tasks."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, VERSION, ABOUT, COMMANDS, ARGUMENTS, EXIT,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            TASK_CREATE, TASK_LIST, TASK_CLEAR
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
