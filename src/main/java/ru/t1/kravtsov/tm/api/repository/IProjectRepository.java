package ru.t1.kravtsov.tm.api.repository;

import ru.t1.kravtsov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void deleteAll();

}
