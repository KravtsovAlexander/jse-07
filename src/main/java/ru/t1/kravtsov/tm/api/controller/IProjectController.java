package ru.t1.kravtsov.tm.api.controller;

public interface IProjectController {

    void displayProjects();

    void createProject();

    void clearProjects();

}
