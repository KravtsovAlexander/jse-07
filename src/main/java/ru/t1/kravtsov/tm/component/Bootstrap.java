package ru.t1.kravtsov.tm.component;

import ru.t1.kravtsov.tm.api.component.IBootstrap;
import ru.t1.kravtsov.tm.api.controller.ICommandController;
import ru.t1.kravtsov.tm.api.controller.IProjectController;
import ru.t1.kravtsov.tm.api.controller.ITaskController;
import ru.t1.kravtsov.tm.api.repository.ICommandRepository;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.service.ICommandService;
import ru.t1.kravtsov.tm.api.service.IProjectService;
import ru.t1.kravtsov.tm.api.service.ITaskService;
import ru.t1.kravtsov.tm.constant.ArgumentConst;
import ru.t1.kravtsov.tm.constant.TerminalConst;
import ru.t1.kravtsov.tm.controller.CommandController;
import ru.t1.kravtsov.tm.controller.ProjectController;
import ru.t1.kravtsov.tm.controller.TaskController;
import ru.t1.kravtsov.tm.repository.CommandRepository;
import ru.t1.kravtsov.tm.repository.ProjectRepository;
import ru.t1.kravtsov.tm.repository.TaskRepository;
import ru.t1.kravtsov.tm.service.CommandService;
import ru.t1.kravtsov.tm.service.ProjectService;
import ru.t1.kravtsov.tm.service.TaskService;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskController taskController = new TaskController(taskService);

    @Override
    public void run(String[] args) {
        parseArguments(args);
        parseCommands();
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;

        final String arg = args[0];
        parseArgument(arg);
        exit();
    }

    private void parseCommands() {
        commandController.displayWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("\nENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;

        switch (arg) {
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                commandController.displayArgumentError();
                System.exit(1);
        }
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;

        switch (command) {
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.displayProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.displayTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                commandController.displayCommandError();
        }
    }

    private void exit() {
        System.exit(0);
    }

}
